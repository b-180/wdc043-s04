public class Main {
    public static void main(String[] args) {
        Driver driver1 = new Driver();
        System.out.println("Details of driver1:");
        driver1.setName("Boy Bawang");
        driver1.setAddress("Bagong Bayan");
        driver1.setAge(30);
        System.out.println(driver1.getName());
        System.out.println(driver1.getAddress());
        System.out.println(driver1.getAge());
        //default constructor
        Car car1 = new Car();
        //parameterized constructor. It has to be coded
        Car car2 = new Car("Tamaraw FX", "Toyota", 40000, driver1);
        Car car3 = new Car();

        Animal animal1 = new Animal();
        animal1.setName("Bantay");
        animal1.setColor("Blue");
        animal1.call();

        car1.setMake("Veyron");
        car1.setBrand("Toyota");
        car1.setPrice(450000);
        System.out.println(car1.getMake());
        System.out.println(car1.getBrand());
        System.out.println(car1.getPrice());

        System.out.println(car2.getMake());
        System.out.println(car2.getBrand());
        System.out.println(car2.getPrice());

//        System.out.println(car1.brand);
//        System.out.println(car1.make);
//        System.out.println(car1.price);
//
//        car1.make = "Veyron";
//        car1.brand = "Bugatti";
//        car1.price = 2000000;
//
//        System.out.println(car1.brand);
//        System.out.println(car1.make);
//        System.out.println(car1.price);

        //Each instance of a class should be independent from
        //one another especially their properties. However,
        //since coming from the same class, they may have the same methods.

        //Cannot add a new property unless done in it's own class

//        car2.make = "Tamraw FX";
//        car2.brand = "Toyota";
//        car2.price = 450000;
//
//        System.out.println(car2.brand);
//        System.out.println(car2.make);
//        System.out.println(car2.price);
//
//        car3.make = "Tamraw";
//        car3.brand = "Toyona";
//        car3.price = 450000;
//
//        System.out.println(car3.make);
//        System.out.println(car3.brand);
//        System.out.println(car3.price);

    }
}
