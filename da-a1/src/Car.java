public class Car {
    //An Object is an idea of a real world object.
    //A Class is the code that describe the Object.
    //An Instance is a tangible copy of an idea instantiated or created from a class.
    //Attributes and Methods
    // public
    //Class attributes/properties must not be public,
    //they should only be and set via class methods called setters and getters
    //private - limit the access and ability to set a variable/method to its class
    //setters are public methods which will allow us to set the value of the attribute of an instance
    //getters are public methods which will allow us to get the value of the attribute of an instance
    private String make, brand;
    private int price;

    //Add a driver variable to add a property which is an object/instance of a Driver class.
    private Driver carDriver;


    //Methods are functions of an object which allows us to perform certain tasks
    //void - means that the function does not return anything
    //Java needs to declare return data types
    public void start(){
        System.out.println("Vroom! Vroom!");
    }

     public void setMake(String make) {
        this.make = make;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setPrice(int price) {
        this.price = price;
    }
//    }

    public String getMake() {
        return make;
    }

    public String getBrand() {
        return brand;
    }

    public int getPrice() {
        return price;
    }

    //constructor is a method which allows us to set the initial values of an instance
    //empty/defualt constructor - default constructor - allows us to create an instance with defualt initizialized values for our properties
    public Car(){
        //empty or you can designate default values instead of getting thme from parameters.
        //Java, actually already creates on for us, however, the empt/defualt constructor made jsut by Java allows us Java to set its own defulat values. If you create your own, you can create your own defualt values.


    }

    //parameterized consturctor - allows us to initialize values to our attributes upon creation of the instance.
    public Car(String make, String brand, int price, Driver driver) {
        this.make = make;
        this.brand = brand;
        this.price = price;
        this.carDriver = driver;
    }

    public Driver getCarDriver() {
        return carDriver;
    }

    public void setCarDriver(Driver carDriver) {
        this.carDriver = carDriver;
    }

    public String getCarDriverName() {
        return this.carDriver.getName();
    }

    public String getCarDriverAddress() {
        return this.carDriver.getAddress();
    }

    public int getCarDriverAge() {
        return this.carDriver.getAge();
    }
}
